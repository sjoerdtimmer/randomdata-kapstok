#Name: Keep Fan On
#Info: Useful if you have a heated bed
#Help: KeepFanOn
#Depend: GCode
#Type: postprocess
#Param: parkX(float:190) Head park X (mm)
#Param: parkY(float:95) Head park Y (mm)
#Param: parkZ(float:150) Head park Z (mm)


## Written by Themba, themba@randomdata.nl
## This script is licensed under the Creative Commons - Attribution - Share Alike (CC BY-SA) terms

import re


with open(filename, "r") as f:
	lines = f.readlines()


with open(filename, "a") as f:
	f.write("; Plugin: start Keep Fan On\n")
	# Move the head to specified location
	f.write("G1 X%f Y%f Z%f F9000\n" % (parkX, parkY, parkZ))
	# fan on
	f.write("M106 S255\n")
	# Wait until the user continues printing
	f.write("M0\n")
	# fan off
	f.write("M106 S0\n")

