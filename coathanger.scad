use <fonts/Write.scad>

$fn=60;

// produce a teardrop shape for printing holes
module teardrop(radius, length, angle) {
	rotate([0, angle, 0]) union() {
		linear_extrude(height = length, center = true, convexity = radius, twist = 0)
			circle(r = radius, center = true);
		linear_extrude(height = length, center = true, convexity = radius, twist = 0)
			projection(cut = false) 
			rotate([0, -angle, 0]) 
			translate([0, 0, radius * sin(45) * 1.5]) 
			cylinder(h = radius * sin(45), r1 = radius * sin(45), r2 = 0, center = true);
	}
}


// the shape that forms the basis of the coat hanger hook thing:
module hookshape(width=12,walllength=80,extendlength=60,smallextend=30,bottomlength=30,angle=45,skewness=0){
	// wall facing part
	square([width,walllength],center=true);

	// large upper hook
	translate([width/2,walllength/2])
	rotate([0,0,angle])
	translate([-width,0])
	multmatrix(m=[ // slightly slant it
		[1,0,0,0],
		[skewness,1,0,-width*skewness],
		[0,0,1,0],
		[0,0,0,1]
		])
	square([width,extendlength]);

	// base thing at the bottom
	translate([-bottomlength,-walllength/2])
	square([bottomlength,width]);

	// small bottom hook
	color([1,0,0])
	translate([-bottomlength,-walllength/2])
	rotate([0,0,angle])
	multmatrix(m=[ // slightly slant it
		[1,0,0,0],
		[skewness,1,0,0],
		[0,0,1,0],
		[0,0,0,1]
		])
	square([width,smallextend]);
}


module rounded_hookshape(r=2,width=12,walllength=80,extendlength=60,smallextend=30,bottomlength=30,angle=45,skewness=0){
	minkowski(){
		hookshape(width=width,bottomlength=bottomlength,walllength=walllength,extendlength=extendlength,smallextend=smallextend,skewness=skewness,angle=angle);
		circle(r=r);
	}
}



//* MAIN:
color([0,0.6,1])
difference(){
	rotate([90,0,0])
	union(){
		for(x=[0:0.05:0.2]){
			linear_extrude(height=12+2*x,center=true)
			rounded_hookshape(r=2-x,width=2,bottomlength=15,walllength=60,extendlength=60,smallextend=20,skewness=0.4,angle=40);
		}
		//linear_extrude(height=14,center=true)
		//rounded_hookshape(r=1,width=2,bottomlength=15,walllength=60,extendlength=60,smallextend=20,skewness=0.4,angle=40);
		//scale([2,2,0.5])
		//sphere(r=1,$fn=10);
	}


	translate([0,0,20])
	rotate([90,0,0])
	teardrop(2,20,90);

	translate([0,0,-5])
	rotate([90,0,0])
	teardrop(2,20,90);

	// screw embeddings top:
	// cone:
	translate([-0.02,0,20])
	rotate([90,0,-90])
	linear_extrude(height=2,scale=1.5)
	tearshape(r=2);
	// head:
	translate([-2.5-0.01,0,20])
	rotate([90,0,0])
	teardrop(3,1,90);	

	// screw embeddings bottom:
	// cone:
	translate([-0.02,0,-5])
	rotate([90,0,-90])
	linear_extrude(height=2,scale=1.5)
	tearshape(r=2);
	// head:
	translate([-2.5-0.01,0,-5])
	rotate([90,0,0])
	teardrop(3,1,90);	


}


//*/


module tearshape(r=1){
	circle(r = r, center = true);
	projection(cut = false) 
	rotate([0, 90, 0]) 
	translate([0, 0, r * sin(45) * 1.5]) 
	cylinder(h = r * sin(45), r1 = r * sin(45), r2 = 0, center = true);
}



/* put my name on it:
translate([-3,-0.5,8])
rotate([90,0,-90])
scale([0.75,0.75,0.6])
write("Themba",font="orbitron.dxf",center=true);
//*/

